<?php

namespace Hestec\UpcomingEvents;

use SilverStripe\Assets\File;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Assets\Image;
use SilverStripe\Security\Permission;

class Event extends DataObject {

    private static $table_name = 'HestecEvent';

    private static $db = array(
        'Title' => 'Varchar(255)',
        'Description' => 'HTMLText',
        'EventDate' => 'Date',
        'EventTime' => 'Time',
        'Location' => 'Varchar(255)',
        'Price' => 'Money'
    );

    private static $has_one = array(
        'EventsPage' => EventsPage::class,
        'Image' => Image::class
    );

    private static $summary_fields = array(
        'EventDate',
        'Title'
    );

    /*public function getCMSFields() {
        //$fields = parent::getCMSFields();

        $ImportBatchFileField = UploadField::create('ImportBatchFile', "ImportBatchFile");


        return new FieldList(
            $ImportBatchFileField
        );

    }*/

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}