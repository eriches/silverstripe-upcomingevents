<?php

namespace Hestec\UpcomingEvents;

use Page;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

class EventsPage extends Page
{

    private static $table_name = 'HestecEventsPage';

    private static $db = array(
    );

    private static $has_many = array(
        'Events' => Event::class
    );


    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $EventsGridField = new GridField(
            'Events',
            _t('UpcomingEvents.EVENTS', "Events"),
            $this->Events(),
            GridFieldConfig_RecordEditor::create()
        );

        $fields->AddFieldsToTab('Root.'._t('UpcomingEvents.EVENTS', "Events"), array(
            $EventsGridField
        ));

        return $fields;

    }

}
