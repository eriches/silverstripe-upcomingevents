<?php

namespace Hestec\UpcomingEvents;

use PageController;

class EventsPageController extends PageController
{

    public function UpComingEvents(){

        $dt = new \DateTime();

        $output = $this->Events()->filter(array('EventDate:GreaterThanOrEqual' => $dt->format('Y-m-d')))->sort('EventDate');

        return $output;

    }

}
