<% include Header %>

<main role="main">
    <div class="container">
        <div class="col-sm-9">
            <h1>$Title hi</h1>
            <div class="content">$Content</div>

            <% loop $Events %>
                $Title
            <% end_loop %>

        </div>
        <% include SideBar %>
    </div>
</main>

<% include Footer %>